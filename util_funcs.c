/**
 * Author: Sanisha Rehan
 * 
 * This file includes utility functions that could be re-used across all the 
 * system software projects.
 */
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>

// Macro to print new lines X numnber of times.
# define NEWLINE(X) { int i=0; for (; i < X; ++i) printf("\n"); }

// Macro to print char X num of times.
# define PRINT_CHAR(ch, X) { int i=0; for(; i < X; ++i) printf("%c", ch);}

/*
 * Check whether a given string is present in array of strings. This function
 * is case-sensitive.
 * 
 * Input arguments: 
 *  arr: pointer 2-D array of characters. Each 1-D array should be null 
 *      terminated, otherwise output cannot be predicted.
 *  L: Length of the 2-arr where each 1-D array would have different width.
 *  str: pointer to string that has to be searched. str should be
 *       null-terminated.
 *
 * Output:
 *  Returns true is str is exact match of one the strings in array of strings.
 *  Otherwise, returns false.
 */
bool IsStringInStringArray(const char* arr[], int L, const char* str) {
    int i;
    for (i = 0; i < L; ++i) {
        int j = 0;
        while(arr[i][j] != '\0' && str[j] != '\0'
                && arr[i][j] == str[j] && j++) {}
        if (arr[i][j] == str[j]) {
            return true;
        }
    }
    return false;
}

/*
 * Returns true if input string starts with the given prefix, else false.
 * 
 * Both strings should be null-terminated, otherwise output cannot be 
 * predicted.
 */
bool IsStartsWith(char* str, char* prefix) {
    int i=0;
    while(str[i] != '\0' && prefix[i] != '\0' && prefix[i] == str[i] && ++i) {}
    if (prefix[i] == '\0') {
        return true;
    }
    return false;
}

/*
 * Parses string and convert into long.
 * Example:
 *   "45" => 45
 *   "0x45" => 69
 *
 * Input string can only be in base 10 or Hex. Only positive numbers are 
 * supported.
 */
long GetLongFromBaseTenOrHexString(char* str) {
    return strtol(str, NULL, 0);
}
