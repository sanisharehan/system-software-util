/*
 * This file includes test cases for util functions.
 */

#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include "util_funcs.c"

void Test_IsStringInStringArray() {
    const char* arr[] = {"load", "save", "increment", "", "subtract"};
    const char* empty_2d_arr[] = {};
    const char* load = "load";
    const char* save = "save";
    const char* not_present = "not_present";
    const char* empty_string = "";
    const char* subtract = "subtract";

    assert(IsStringInStringArray(arr, 5, load));
    assert(IsStringInStringArray(arr, 5, save));
    assert(IsStringInStringArray(arr, 5, subtract));
    assert(IsStringInStringArray(arr, 5, empty_string));
    assert(!IsStringInStringArray(arr, 5, not_present));

    assert(!IsStringInStringArray(empty_2d_arr, 0, load));
    assert(!IsStringInStringArray(empty_2d_arr, 0, save));
    assert(!IsStringInStringArray(empty_2d_arr, 0, subtract));
    assert(!IsStringInStringArray(empty_2d_arr, 0, not_present));
    assert(!IsStringInStringArray(empty_2d_arr, 0, empty_string));
}

void Test_IsStartsWith() {
    // Success.
    assert(IsStartsWith("loading", "load"));
    assert(IsStartsWith("load", ""));
    assert(IsStartsWith("load", "load"));

    // Failure.
    assert(!IsStartsWith("", "load"));
    assert(!IsStartsWith("loading", "oad"));
    assert(!IsStartsWith("loading", "moad"));
    assert(!IsStartsWith("loading", "ing"));
}

void Test_GetLongFromBaseTenOrHexString() {
    assert(GetLongFromBaseTenOrHexString("100") == 100);
    assert(GetLongFromBaseTenOrHexString("0") == 0);
    assert(GetLongFromBaseTenOrHexString("0x0") == 0);
    assert(GetLongFromBaseTenOrHexString("0x10") == 16);
    assert(GetLongFromBaseTenOrHexString("0x45") == 69);
}

int main() {
    PRINT_CHAR('=', 20); NEWLINE(2);
    Test_IsStringInStringArray();
    printf("IsStringInStringArray - All tests passed.");
    NEWLINE(2);

    PRINT_CHAR('=', 20); NEWLINE(2);
    Test_IsStartsWith();
    printf("IsStartsWith - All tests passed.");
    NEWLINE(2);

    PRINT_CHAR('=', 20); NEWLINE(2);
    Test_GetLongFromBaseTenOrHexString();
    printf("GetLongFromBaseTenOrHexString - All tests passed.");
    NEWLINE(2);

    PRINT_CHAR('=', 20); NEWLINE(2);
    return 0;
}
